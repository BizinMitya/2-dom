import org.w3c.dom.*;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Dmitriy on 02.03.2016.
 */
public class ReaderDOM {
    private TransformerFactory transformerFactory;
    private Document document;
    private Transformer transformer;
    private float mark = 0;
    private float numberOfSubjects = 0;
    private boolean flag = false;

    public ReaderDOM(Document document) throws TransformerConfigurationException {
        this.document = document;
        transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 4);
        transformer = transformerFactory.newTransformer();
    }

    public void checkAverage() throws TransformerException, FileNotFoundException {
        NodeList students = document.getElementsByTagName("student");
        NodeList nodeList = null;
        Node node = null;
        for (int i = 0; i < students.getLength(); i++) {
            nodeList = students.item(i).getChildNodes();
            flag = false;
            for (int j = 0; j < nodeList.getLength(); j++) {
                node = nodeList.item(j);
                if (node.getNodeName().equals("subject")) {
                    numberOfSubjects++;
                    mark += Float.parseFloat(node.getAttributes().getNamedItem("mark").getTextContent());
                }
                if (node.getNodeName().equals("average")) {
                    flag = true;
                    if (Float.parseFloat(node.getTextContent()) != mark / numberOfSubjects) {
                        node.setTextContent(String.valueOf(mark / numberOfSubjects));
                    }
                }
            }
            if (!flag) {
                Text a = document.createTextNode(String.valueOf(mark / numberOfSubjects));
                Element p = document.createElement("average");
                p.appendChild(a);
                students.item(i).appendChild(p);
            }
            mark = 0;
            numberOfSubjects = 0;
        }
        write(new FileOutputStream("out.xml"));
    }

    public void write(OutputStream outputStream) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");//отступы
        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "group.dtd");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");//отступы
        transformer.transform(new DOMSource(document), new StreamResult(outputStream));
    }
}